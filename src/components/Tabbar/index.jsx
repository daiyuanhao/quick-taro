import Taro from '@tarojs/taro'
import { Component } from 'react'
import { View, Image } from '@tarojs/components'

import './index.scss'

class Tabbar extends Component {
  
  render() {
    const tabbar = [
      {pagePath: 'pages/index/index', iconPath: 'https://qn-qghotel.lindingtechnology.com/pcenter_1629109399851.png', text: '进出预约'},
      {pagePath: 'pages/passRecord/index', iconPath: 'https://qn-qghotel.lindingtechnology.com/pcenter_1629108903655.png', text: '通行记录'},
      {pagePath: 'pages/mine/index', iconPath: 'https://qn-qghotel.lindingtechnology.com/pcenter_1629108907890.png', text: '系统登录'},
    ]
    return (
      <View className='tabbar'>
        {tabbar.map((item, index) => (
          <View key={index} className='tab' onClick={()=>{Taro.switchTab({url: '/'+item.pagePath})}}>
            <Image className='icon' src={item.iconPath}></Image>
            <View className='text'>{ item.text }</View>
          </View>
        ))}
      </View>
    )
  }
}

export default Tabbar
import { get } from '@utils/request'

// 更新用户信息
export const dispatchUpdateUserInfo = () => {
  return async dispatch => {
    const { data: userInfo } = await get('/weapp/me')
    console.log(dispatch)
    dispatch({
      type: 'CHANGE_USER_INFO',
      userInfo
    })
    return userInfo
  }
}

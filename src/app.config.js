export default {
  pages: [
    'pages/index/index',
    'pages/mine/index'
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
  },
  tabBar: {
    color: '#C0C5CE',
    selectedColor: '#525D70',
    borderStyle: 'white',
    list: [{
      pagePath: 'pages/index/index',
      text: '首页',
    }, {
      pagePath: 'pages/mine/index',
      text: '我的',
    }]
  }
}

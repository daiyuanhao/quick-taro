import Taro from '@tarojs/taro'
import { Component } from 'react'
import { Provider } from 'react-redux'
import { get } from '@utils/request'
import { updateWeapp } from '@utils/index'

import { store } from './store'
import './app.scss'

class App extends Component {

  componentDidMount() {
    console.log(Taro.getAccountInfoSync())
    
    // 系统信息初始化
    this.initLayout()
    
    // 小程序更新版本检查
    updateWeapp()
    
    // 初始化用户信息
    this.initUserInfo()
  }

  async initLayout() {
    const res = Taro.getSystemInfoSync()
    store.dispatch({ type: 'INIT_LAYOUT', layout: {
      safeArea_bottom: res.screenHeight - res.safeArea.bottom
    }})
  }

  async initUserInfo() {
    const { data } = await get('/weapp/me')
    store.dispatch({ type: 'CHANGE_USER_INFO', userInfo: data })
  }

  // 在 App 类中的 render() 函数没有实际作用
  // 请勿修改此函数
  render () {
    return (
      <Provider store={store}>
        {this.props.children}
      </Provider>
    )
  }
}

export default App

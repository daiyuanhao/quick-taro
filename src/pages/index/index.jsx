import Taro from '@tarojs/taro'
import { Component } from 'react'
import { View, Text, Image } from '@tarojs/components'
import { connect } from 'react-redux'
import LdPage from '@components/LdPage'
import {setTabbar} from '@utils/index'
import './index.scss'

@connect(state => state.app)
class Index extends Component {

  render() {
    return (
      <View>hello world!</View>
    )
  }
}

export default Index


import Taro from '@tarojs/taro'
import { Component } from 'react'
import { View } from '@tarojs/components'
import { connect } from 'react-redux'
import LdPage from '@components/LdPage'
import * as appActions from '@actions/app'
import './index.scss'

@connect(state => state.app, {...appActions})
class Mine extends Component {

  componentDidMount() {
    this.LdPage.pageInit()
  }

  render() {
    return (
      <LdPage onRef={ref => this.LdPage = ref}>
        <View>hello mine!</View>
      </LdPage>
    )
  }
}

export default Mine


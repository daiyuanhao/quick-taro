import Taro from '@tarojs/taro'

export function setTabbar() {
  Taro.setTabBarItem({
    index: 0,
    iconPath: 'https://qn-qghotel.lindingtechnology.com/pcenter_1629109399851.png',
    selectedIconPath: 'https://qn-qghotel.lindingtechnology.com/pcenter_1629108899776.png'
  })
  Taro.setTabBarItem({
    index: 1,
    iconPath: 'https://qn-qghotel.lindingtechnology.com/pcenter_1629108903655.png',
    selectedIconPath: 'https://qn-qghotel.lindingtechnology.com/pcenter_1629109403603.png'
  })
  Taro.setTabBarItem({
    index: 2,
    iconPath: 'https://qn-qghotel.lindingtechnology.com/pcenter_1629108907890.png',
    selectedIconPath: 'https://qn-qghotel.lindingtechnology.com/pcenter_1629109407317.png'
  })
}

export function updateWeapp() {
  if (Taro.canIUse('getUpdateManager')) {
    const updateManager = Taro.getUpdateManager()
    updateManager.onCheckForUpdate(() => {
      console.log('checking.......')
    })
    updateManager.onUpdateReady(() => {
      // noinspection JSIgnoredPromiseFromCall
      Taro.showModal({
        title: '更新提示',
        content: '新版本已经准备好，是否重启应用？',
        success: async function(res) {
          if (res.confirm) {
            updateManager.applyUpdate()
          }
        }
      })
    })
    updateManager.onUpdateFailed(() => {
      // noinspection JSIgnoredPromiseFromCall
      Taro.showModal({
        title: '更新提示',
        content: '新版本下载失败，请检查你的微信',
        showCancel: false
      })
    })
  } else {
    // noinspection JSIgnoredPromiseFromCall
    Taro.showModal({
      title: '微信升级',
      content: '当前微信版本过低，部分功能无法使用，请升级到最新版本',
      showCancel: false
    })
  }
}
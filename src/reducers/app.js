const INITIAL_STATE = {
  layout: {},
  userInfo: {},
  qiniuConfig: {}
}

export default function counter (state = INITIAL_STATE, action) {
  switch (action.type) {
    case 'INIT_LAYOUT':
      return {
        ...state,
        layout: action.layout
      }
    case 'CHANGE_USER_INFO':
      return {
        ...state,
        userInfo: action.userInfo
      }
    case 'SET_QINIU_CONFIG':
      return {
        ...state,
        qiniuConfig: action.qiniuConfig
      }
    default:
      return state
  }
}
